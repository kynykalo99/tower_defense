﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towershoot : MonoBehaviour
{
    [Tooltip("referencia al prefab de la bala")]
    public GameObject bulletprefab;

    public Transform enemyToShot;
    [Tooltip("referencia al enemigo al que quiero disparar")]
    public float timeLastShot;

    
  

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        { 
            GameObject bullet = Instantiate(bulletprefab, transform.position, Quaternion.identity);
            bullet.GetComponent<BulletMovement>().enemy = enemyToShot;

            timeLastShot = Time.time;
        }
       
          
    }
   
}
