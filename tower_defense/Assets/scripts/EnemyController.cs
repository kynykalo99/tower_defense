﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [Tooltip("posicion a la que queremeos que se mueva el enemigo")]

    public Vector3 targetPosition;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<NavMeshAgent>().SetDestination(targetPosition);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
