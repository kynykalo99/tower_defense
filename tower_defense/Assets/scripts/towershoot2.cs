﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towershoot2 : MonoBehaviour
{
    [Tooltip("referencia al prefab de la bala")]
    public GameObject bulletprefab2;

    public Transform enemyToShot;
    [Tooltip("referencia al enemigo al que quiero disparar")]
    public float timeLastShot;

    public BoxCollider rangoTorre2;
  

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bulletTwo = Instantiate(bulletprefab2, transform.position, Quaternion.identity);
            bulletTwo.GetComponent<BulletMovement>().enemy = enemyToShot;

            timeLastShot = Time.time;
        }
        
       
          
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (rangoTorre2)
        {
            GameObject bulletTwo = Instantiate(bulletprefab2, transform.position, Quaternion.identity);
            bulletTwo.GetComponent<BulletMovement>().enemy = enemyToShot;

            timeLastShot = Time.time;
        }
    }

}
