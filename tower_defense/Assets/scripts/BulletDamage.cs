﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    public float damage;

    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("enemy")) 
        {
            collision.gameObject.GetComponent<Enemylife>().enemyLife-=damage;
        }

        Destroy(gameObject);
      
    }
}
