﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [Tooltip("velocidada de la bala")]
    public float bulletSpeed;
    public Transform enemy;

   
    private Transform target;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, enemy.position, bulletSpeed);
        }else
        {
            Destroy(gameObject);
        }
    }

    
  
}
